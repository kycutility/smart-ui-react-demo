import axios from "axios";
import { useEffect, useState } from "react";
import FrankieSmartUI from "./FrankieSmartUi";

const FRANKIE_URL = "https://backend.demo.frankiefinancial.io";
const FRANKIE_CONFIG = {
  frankieBackendUrl: "https://backend.demo.frankiefinancial.io",
  documentTypes: [
    "NATIONAL_HEALTH_ID",
    { type: "PASSPORT", acceptedCountries: "ALL" },
    "DRIVERS_LICENCE"],
  idScanVerification: false,
  checkProfile: "auto",
  maxAttemptCount: 5,
  googleAPIKey: false,
  phrases: {
    document_select: {
      title: "Custom Text Here: Choose your ID",
      hint_message: "Choose which ID you'd like to provide."
    },
  },
  requestAddress: { acceptedCountries: ["AUS", "NZL"] },
  consentText:
    "I agree with the terms described in the Consent section of the Company's webpage",
};

/**
 ***** SECRET CREDENTIALS
 ***** 👇 DO NOT COMMIT OR PUBLISH THESE VALUES 👇
 */

const CUSTOMER_ID = "";
const CUSTOMER_CHILD_ID = "";
const API_KEY = "";

/**
 ****** 👆 DO NOT COMMIT OR PUBLISH THESE VALUES 👆
 ****** SECRET CREDENTIALS
 */

/* ---------------------------------------------- */

/**
 * Creating a session
 */

// Join credentials with ":"
// If you don't have a child id, don't include it when joining
const JOINED_CREDENTIALS = [CUSTOMER_ID, CUSTOMER_CHILD_ID, API_KEY].filter(Boolean).join(":");
// Encode resulting string to base64
const ENCODED_CREDENTIALS = window.btoa(JOINED_CREDENTIALS); // use your base64 encoded credentials!
// Add the encoded string to the "authorization" header, prefixed with the "machine" auth-scheme
const FRANKIE_HEADERS = { authorization: `machine ${ENCODED_CREDENTIALS}` };

function App() {
  const [start, setStart] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const initialiseWidget = async () => {
      const data =
        await axios.post(`${FRANKIE_URL}/auth/v1/machine-session`, {}, {
          headers: FRANKIE_HEADERS,
        }
        )
      const {
        headers: { token: ffToken },
      } = data;
      window.frankieFinancial.initialiseOnboardingWidget({
        applicantReference: "test", // This will be your applicants reference code and corresponds to the customer_reference value in the FrankieOne Core API 
        config: FRANKIE_CONFIG,
        ffToken,
        width: `${window.innerWidth * 0.8}px`,
        height: `${window.innerHeight / 1.5}px`,
      });
    };

    if (window.frankieFinancial && !start) {
      initialiseWidget();
    }
  }, [start]);

  return (
    <div>
      <button
        style={{
          width: "50%",
          margin: "auto",
          marginTop: "25%",
        }}
        onClick={() => {
          setOpen(true);
          setStart(true);
        }}
      >
        {start ? `Continue!` : `Start Frankie Smart UI!`}
      </button>
      <FrankieSmartUI show={open} onClose={() => setOpen(false)} />
    </div>
  );
}

export default App;
