# Frankie React Smart UI Demo

## To Start

In the project directory, run:
### `npm install`
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## How it Works

Load the script inside your index.html

```
<script src="https://assets.frankiefinancial.io/onboarding/v4/ff-onboarding-widget.umd.min.js"></script>
```

In your parent component initialise the widget with the appropriate configuration.

Don't forget to fill in your base64 encoded credentials, and remove demo from the config object!

### With "idScanVerification"
Just keep in mind camera capture will only work when serving your application using HTTPS.
If not able to run HTTPS for any reason, simply remove `HTTPS=true` from the `start` script in `package.json`.